//
//  AES256.swift
//
//  Created by Saran Nonkamjan on 29/4/2562 BE.
//  Copyright © 2562 Saran Nonkamjan. All rights reserved.
//

import CommonCrypto
import Foundation

public struct AES256 {

    private var key: Data
    private var iv: Data

    public init(key: Data, iv: Data) throws {
        guard key.count == kCCKeySizeAES256 else {
            throw Error.badKeyLength
        }
        guard iv.count == kCCBlockSizeAES128 else {
            throw Error.badInputVectorLength
        }
        self.key = key
        self.iv = iv
    }

    public enum Error: Swift.Error {
        case keyGeneration(status: Int)
        case cryptoFailed(status: CCCryptorStatus)
        case badKeyLength
        case badInputVectorLength
    }

    public func encrypt(_ digest: Data) throws -> Data {
        return try crypt(input: digest, operation: CCOperation(kCCEncrypt))
    }

    public func decrypt(_ encrypted: Data) throws -> Data {
        return try crypt(input: encrypted, operation: CCOperation(kCCDecrypt))
    }

    private func crypt(input: Data, operation: CCOperation) throws -> Data {
        var outLength = Int(0)
        var outBytes = [UInt8](repeating: 0, count: input.count + kCCBlockSizeAES128)
        var status: CCCryptorStatus = CCCryptorStatus(kCCSuccess)

//        let encryptedBytes = input.withUnsafeBytes { $0.load(as: UnsafePointer<UInt8>.self) }
//        let ivBytes = iv.withUnsafeBytes { $0.load(as: UnsafePointer<UInt8>.self) }
//        let keyBytes = key.withUnsafeBytes { $0.load(as: UnsafePointer<UInt8>.self) }

        input.withUnsafeBytes { (encryptedBytes: UnsafePointer<UInt8>!) -> () in
            iv.withUnsafeBytes { (ivBytes: UnsafePointer<UInt8>!) in
                key.withUnsafeBytes { (keyBytes: UnsafePointer<UInt8>!) -> () in
                    status = CCCrypt(operation,
                                     CCAlgorithm(kCCAlgorithmAES),            // algorithm
                        CCOptions(kCCOptionPKCS7Padding),           // options
                        keyBytes,                                   // key
                        key.count,                                  // keylength
                        ivBytes,                                    // iv
                        encryptedBytes,                             // dataIn
                        input.count,                                // dataInLength
                        &outBytes,                                  // dataOut
                        outBytes.count,                             // dataOutAvailable
                        &outLength)                                 // dataOutMoved
                }
            }
        }
        guard status == kCCSuccess else {
            throw Error.cryptoFailed(status: status)
        }
        return Data(bytes: UnsafePointer<UInt8>(outBytes), count: outLength)
    }

    public static func createKey(password: Data, salt: Data) throws -> Data {
        let length = kCCKeySizeAES256
        var status = Int32(0)
        var derivedBytes = [UInt8](repeating: 0, count: length)

//        let passwordBytes = password.withUnsafeBytes { $0.load(as: UnsafePointer<Int8>.self) }
//        let saltBytes = salt.withUnsafeBytes { $0.load(as: UnsafePointer<UInt8>.self) }

        password.withUnsafeBytes { (passwordBytes: UnsafePointer<Int8>!) in
            salt.withUnsafeBytes { (saltBytes: UnsafePointer<UInt8>!) in
                status = CCKeyDerivationPBKDF(CCPBKDFAlgorithm(kCCPBKDF2),                  // algorithm
                    passwordBytes,                                // password
                    password.count,                               // passwordLen
                    saltBytes,                                    // salt
                    salt.count,                                   // saltLen
                    CCPseudoRandomAlgorithm(kCCPRFHmacAlgSHA1),   // prf
                    10000,                                        // rounds
                    &derivedBytes,                                // derivedKey
                    length)                                       // derivedKeyLen
            }
        }
        guard status == 0 else {
            throw Error.keyGeneration(status: Int(status))
        }
        return Data(bytes: UnsafePointer<UInt8>(derivedBytes), count: length)
    }

    public static func randomIv() -> Data {
        return randomData(length: kCCBlockSizeAES128)
    }

    public static func randomSalt() -> Data {
        return randomData(length: 8)
    }

    public static func randomData(length: Int) -> Data {
        var data = Data(count: length)

        let status = data.withUnsafeMutableBytes { mutableBytes in
            SecRandomCopyBytes(kSecRandomDefault, length, mutableBytes)
        }
        assert(status == Int32(0))
        return data
    }
}

/*
let digest = "hello world".data(using: .utf8)!
let password = "ShIńė0ut"
let salt = AES256.randomSalt()
let iv = AES256.randomIv()
let key = try AES256.createKey(password: password.data(using: .utf8)!, salt: salt)
let aes = try AES256(key: key, iv: iv)
let encrypted = try aes.encrypt(digest)
let decrypted = try aes.decrypt(encrypted)

print("Encrypted: \(encrypted.hexString)")
print("Decrypted: \(decrypted.hexString)")
print("Password: \(password)")
print("Key: \(key.hexString)")
print("IV: \(iv.hexString)")
print("Salt: \(salt.hexString)")
print(" ")
*/
