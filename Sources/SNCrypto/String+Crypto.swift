//
//  File.swift
//  
//
//  Created by Saran Nonkamjan on 23/9/2562 BE.
//  Copyright © 2562 Saran Nonkamjan. All rights reserved.
//

import Foundation
import CommonCrypto

public extension String {
    func SHA256() -> String {
        let data = self.data(using: String.Encoding.utf8)
        let res = NSMutableData(length: Int(CC_SHA256_DIGEST_LENGTH))
        CC_SHA256(((data! as NSData)).bytes, CC_LONG(data!.count), res?.mutableBytes.assumingMemoryBound(to: UInt8.self))
        let hashedString = "\(res!)".replacingOccurrences(of: "", with: "").replacingOccurrences(of: " ", with: "")
        let badchar: CharacterSet = CharacterSet(charactersIn: "\"<\",\">\"")
        let cleanedstring: String = (hashedString.components(separatedBy: badchar) as NSArray).componentsJoined(by: "")
        return cleanedstring
    }
}
