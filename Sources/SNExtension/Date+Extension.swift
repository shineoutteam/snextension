//
//  Date+Extension.swift
//
//  Created by Saran Nonkamjan on 1/3/2559 BE.
//  Copyright © 2559 Saran Nonkamjan. All rights reserved.
//

import Foundation
import SNLanguage

public var timeZoneSecFromServer: NSInteger  = 0

public extension Date
{
    func isBefore(_ date:Date) -> Bool{

		return self.isInRange(Date(timeIntervalSince1970: TimeInterval(0)), to: date)
	}

    func isInRange(_ from: Date, to:Date) -> Bool{
		if(self.compare(from) == ComparisonResult.orderedDescending || self.compare(from) == ComparisonResult.orderedSame){
			if(self.compare(to) == ComparisonResult.orderedAscending || self.compare(to) == ComparisonResult.orderedSame){
				// date is in range
				return true
			}
		}
		// date is not in range
		return false
	}

    func printFormatter(formatter:String, identifier: Calendar.Identifier, localeIdentifier: String? = Langauge.sharedObject.langauge.localizedString) -> String {

        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        format.dateFormat = formatter
        format.locale = Locale(identifier: localeIdentifier ?? "")

        return format.string(from: self as Date)
    }

    func printddMMyy(identifier: Calendar.Identifier, localeIdentifier: String? = Langauge.sharedObject.langauge.localizedString) -> String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        format.dateFormat = "dd-MM-yy"
        format.locale = Locale(identifier: localeIdentifier ?? "")

        return format.string(from: self as Date)
    }

    func printddMMMyy(identifier: Calendar.Identifier, localeIdentifier: String? = Langauge.sharedObject.langauge.localizedString) -> String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        format.dateFormat = "d MMM yy"
        format.locale = Locale(identifier: localeIdentifier ?? "")

        return format.string(from: self as Date)
    }

    func printddMMMyyyy(identifier: Calendar.Identifier, localeIdentifier: String? = Langauge.sharedObject.langauge.localizedString) -> String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        format.dateFormat = "d MMM yyyy"
        format.locale = Locale(identifier: localeIdentifier ?? "")

        return format.string(from: self as Date)
    }

    func printMMM_yyyy(identifier: Calendar.Identifier) -> String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        format.dateFormat = "MMM yyyy"
        format.locale = Locale(identifier: Langauge.sharedObject.langauge.localizedString)

        return format.string(from: self as Date)
    }

    func printMMM(identifier: Calendar.Identifier) -> String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        format.dateFormat = "MMM"
        format.locale = Locale(identifier: Langauge.sharedObject.langauge.localizedString)

        return format.string(from: self as Date)
    }

    func printMMM_yy(identifier: Calendar.Identifier) -> String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        format.dateFormat = "MMM yy"
        format.locale = Locale(identifier: Langauge.sharedObject.langauge.localizedString)

        return format.string(from: self as Date)
    }

    func printHHmm(identifier: Calendar.Identifier) -> String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        format.dateFormat = "HH:mm"
        format.locale = Locale(identifier: Langauge.sharedObject.langauge.localizedString)

        return format.string(from: self as Date)
    }

    var printYYYY_MM_DD: String {

        let formatter = DateFormatter()
        let calendar = Calendar(identifier:Calendar.Identifier.gregorian)
        formatter.calendar = calendar
        formatter.dateFormat = "yyyy-MM-dd"

        return formatter.string(from: self as Date)
    }

    var printISO8601: String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        format.calendar = calendar
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        format.locale = enUSPosixLocale
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"

        let iso8601String = format.string(from: self as Date)
        return iso8601String
    }

    var printISO8601Short: String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        format.calendar = calendar
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        format.locale = enUSPosixLocale
        format.dateFormat = "yyyy-MM-dd"

        let iso8601String = format.string(from: self as Date)
        return iso8601String
    }

    var serverISO8601Short: String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        format.calendar = calendar
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        format.locale = enUSPosixLocale
        format.timeZone = TimeZone(secondsFromGMT: timeZoneSecFromServer)
        format.dateFormat = "yyyy-MM-dd"

        let iso8601String = format.string(from: self as Date)
        return iso8601String
    }

    var printISO8601UTC: String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        format.calendar = calendar
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        format.locale = enUSPosixLocale
        format.timeZone = TimeZone(secondsFromGMT: 0)
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.ZZZZZ"

        let iso8601String = format.string(from: self as Date)
        return iso8601String
    }

    var printISO8601UTCShort: String {
        let format = DateFormatter()
        let calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        format.calendar = calendar
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        format.locale = enUSPosixLocale
        format.timeZone = TimeZone(secondsFromGMT: 0)
        format.dateFormat = "yyyy-MM-dd"

        let iso8601String = format.string(from: self as Date)
        return iso8601String
    }

    func printISO8601UTC(formatter:String, identifier: Calendar.Identifier, localeIdentifier: String? = Langauge.sharedObject.langauge.localizedString) -> String {

        let format = DateFormatter()
        let calendar = Calendar(identifier: identifier)
        format.calendar = calendar
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        format.locale = enUSPosixLocale
        format.timeZone = TimeZone(secondsFromGMT: 0)
        format.dateFormat = formatter

        let iso8601String = format.string(from: self as Date)
        return iso8601String
    }

    var prettyFormat: String {
        let calendar: Calendar = Calendar(identifier:Calendar.Identifier.gregorian)

        let dateFormatter = DateFormatter()
        dateFormatter.calendar = calendar
        dateFormatter.dateFormat = "d MMM yyyy"

        let timeFormatter = DateFormatter()
        timeFormatter.calendar = calendar
        timeFormatter.dateFormat = "HH:mm"

        let componentsCompare = calendar.dateComponents([.minute, .hour, .day, .month, .year], from: self, to: Date())

        let componentsToday = calendar.dateComponents([.day, .month, .year], from: Date())
        let componentsCheck = calendar.dateComponents([.day, .month, .year], from: self)

        if componentsCompare.year! >= 1 {
            return dateFormatter.string(from: self)
//            return "\(componentsCompare.year ?? 0) \(Langauge.localizedString(withKey: "ปีก่อน"))"
        } else if componentsCompare.month! >= 1 {
            return dateFormatter.string(from: self)
//            return "\(componentsCompare.month ?? 0) \(Langauge.localizedString(withKey: "เดือนก่อน"))"
        } else if (componentsCompare.day! >= 1) {
            return dateFormatter.string(from: self)
//            return "\(componentsCompare.day ?? 0) \(Langauge.localizedString(withKey: "วันก่อน"))"
        }

        if componentsToday.day! - componentsCheck.day! == 0 {
            // Today
//            if componentsCompare.minute == 0 && componentsCompare.hour == 0 {
//                return Langauge.localizedString(withKey: "เดี่ยวนี้")
//            } else if componentsCompare.minute! >= 1 && componentsCompare.hour == 0 {
//                return "\(componentsCompare.minute ?? 0) \(Langauge.localizedString(withKey: "นาทีที่แล้ว"))"
//            }
//
//            return "\(Langauge.localizedString(withKey: "วันนี้")) \(timeFormatter.string(from: self))"
            return Langauge.localizedString(withKey: "Today")
        } else if componentsToday.day! - componentsCheck.day! == 1 {
            return Langauge.localizedString(withKey: "Yesterday")
        }

        return dateFormatter.string(from: self)
    }

    func componantDMY(identifier: Calendar.Identifier) -> (day: Int?, month: Int?, year: Int?) {
        let calendar = Calendar(identifier:identifier)
        let componentsCheck = calendar.dateComponents([.day, .month, .year], from: self)

        return (componentsCheck.day, componentsCheck.month, componentsCheck.year)
    }

    func componantHMDMY(identifier: Calendar.Identifier) -> (minute: Int?, hour: Int?, day: Int?, month: Int?, year: Int?) {
        let calendar = Calendar(identifier:identifier)
        let componentsCheck = calendar.dateComponents([.minute, .hour, .day, .month, .year], from: self)

        return (componentsCheck.minute, componentsCheck.hour, componentsCheck.day, componentsCheck.month, componentsCheck.year)
    }

    func componantHM(toDate: Date, identifier: Calendar.Identifier) -> (minute: Int?, hour: Int?) {
        let calendar = Calendar(identifier:identifier)
        let componentsCheck = calendar.dateComponents([.minute, .hour], from: self, to: toDate)

        return (componentsCheck.minute, componentsCheck.hour)
    }
}

public extension String {
    /**
     *  @param Convent String to date Identifier alway format from gregorian
     */
    func date(formatter: String, identifier: Calendar.Identifier = .gregorian) -> Date? {

        let dateFormatter = DateFormatter()
        var calendar = Calendar(identifier:identifier)
        calendar.timeZone = TimeZone(secondsFromGMT: timeZoneSecFromServer) ?? TimeZone.current
        dateFormatter.timeZone = calendar.timeZone
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.calendar = calendar
        dateFormatter.dateFormat = formatter

        return dateFormatter.date(from: self)
    }

    var dateiso8601_1: Date? {
        let dateFormatter = DateFormatter()
        let calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        dateFormatter.calendar = calendar
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"

        return dateFormatter.date(from: self)
    }

    var dateiso8601: Date? {
        let dateFormatter = DateFormatter()
        let calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        dateFormatter.calendar = calendar
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.ZZZZZ"

        return dateFormatter.date(from: self)
    }

    var dateYYYY_MM_DD: Date? {

        let formatter = DateFormatter()
        var calendar = Calendar(identifier:Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: timeZoneSecFromServer) ?? TimeZone.current
        formatter.timeZone = calendar.timeZone
        formatter.calendar = calendar
        formatter.dateFormat = "yyyy-MM-dd"

        return formatter.date(from: self)
    }

    var dateYYYY_MM_DD_HH_MM_SS: Date? {

        let formatter = DateFormatter()
        var calendar = Calendar(identifier:Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: timeZoneSecFromServer) ?? TimeZone.current
        formatter.timeZone = calendar.timeZone
        formatter.calendar = calendar
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        return formatter.date(from: self)
    }

    var dateYYYYMM: Date? {

        let formatter = DateFormatter()
        var calendar = Calendar(identifier:Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: timeZoneSecFromServer) ?? TimeZone.current
        formatter.timeZone = calendar.timeZone
        formatter.calendar = calendar
        formatter.dateFormat = "yyyyMM"

        return formatter.date(from: self)
    }
}
