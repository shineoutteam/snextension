//
//  UIImage+Delegate.swift
//
//  Created by Saran Nonkamjan on 20/1/2562 BE.
//  Copyright © 2562 ShineOut. All rights reserved.
//

#if canImport(UIKit)

import Foundation
import Photos
import UIKit

public var FOLDER_NAME: String = "ShineOut"

public protocol SaveToCameraRollDelegate {
    func image(image: UIImage, didFinishSavingWithError error: Error?)
}

public class SaveImage: NSObject {

    var image: UIImage?

    init(newImage: UIImage) {
        super.init()

        image = newImage
    }

    // MARK: - Save To Camera Roll

    public func requestAuthorizationHandler(status: PHAuthorizationStatus) {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            // ideally this ensures the creation of the photo album even if authorization wasn't prompted till after init was done
            print("trying again to create the album")
            self.createAlbum()
        } else {
            print("should really prompt the user to let them know it's failed")
        }

    }

    public func createAlbum() {
        guard fetchAssetCollectionForAlbum() == nil else {
            return
        }


        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: FOLDER_NAME)   // create an asset collection with the album name
        }) { success, error in
            if success {
                //                let assetCollection = fetchAssetCollectionForAlbum()
            } else {
                print("error \(String(describing: error))")
            }
        }
    }

    public func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", FOLDER_NAME)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)

        if let _: AnyObject = collection.firstObject {
            return collection.firstObject
        }
        return nil
    }

    public func authorizationAndsaveToCameraRoll(delegate: SaveToCameraRollDelegate) {
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                ()
            })
        }

        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            self.createAlbum()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                self.saveToCameraRoll(delegate: delegate)
            }
        } else {
            PHPhotoLibrary.requestAuthorization(requestAuthorizationHandler)
        }
    }

    public func saveToCameraRoll(delegate: SaveToCameraRollDelegate) {
        let callback = delegate

        guard let assetCollection = fetchAssetCollectionForAlbum(), let saveImage = image else {
            return
        }

        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: saveImage)
            let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: assetCollection)
            let enumeration: NSArray = [assetPlaceHolder!]
            albumChangeRequest!.addAssets(enumeration)
        }) { (success, error) in
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                callback.image(image: saveImage, didFinishSavingWithError: error)
            }
        }
    }
}

#endif
