//
//  UICollectionViewCell+extension.swift
//
//  Created by Saran Nonkamjan on 2/19/2560 BE.
//  Copyright © 2560 ShineOut. All rights reserved.
//

#if canImport(UIKit)

import Foundation
import UIKit

public extension UICollectionViewCell {
    class var identifier: String { return String.className(self) }
}

#endif
