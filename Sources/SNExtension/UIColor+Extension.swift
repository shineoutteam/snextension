//
//  UIColor+Extension.swift
//
//  Created by Saran Nonkamjan on 1/3/2559 BE.
//  Copyright © 2559 Saran Nonkamjan. All rights reserved.
//

#if canImport(UIKit)

import Foundation
import UIKit

public extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
		assert(red >= 0 && red <= 255, "Invalid red component")
		assert(green >= 0 && green <= 255, "Invalid green component")
		assert(blue >= 0 && blue <= 255, "Invalid blue component")

		self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
	}

    convenience init(netHex:Int) {
		self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
	}

    convenience init(rgba: String) {
		var red:   CGFloat = 0.0
		var green: CGFloat = 0.0
		var blue:  CGFloat = 0.0
		var alpha: CGFloat = 1.0

		if rgba.hasPrefix("#") {
			let index   = rgba.index(rgba.startIndex, offsetBy: 1)
			let hex     = rgba.substring(index.utf16Offset(in: rgba)) //rgba.substring(index.encodedOffset)
			let scanner = Scanner(string: hex)
			var hexValue: CUnsignedLongLong = 0
			if scanner.scanHexInt64(&hexValue) {
				switch (hex.count) {
				case 3:
					red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
					green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
					blue  = CGFloat(hexValue & 0x00F)              / 15.0
				case 4:
					red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
					green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
					blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
					alpha = CGFloat(hexValue & 0x000F)             / 15.0
				case 6:
					red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
					green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
					blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
				case 8:
					red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
					green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
					blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
					alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
				default:
					print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8")
				}
			} else {
				print("Scan hex error")
			}
		} else {
			print("Invalid RGB string, missing '#' as prefix")
		}
		self.init(red:red, green:green, blue:blue, alpha:alpha)
	}

    class func withColor(_ color: UIColor, size: CGSize) -> UIImage? {

        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)

        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }

    func hex() -> String {
        var red         :   CGFloat  =   255.0
        var green       :   CGFloat  =   255.0
        var blue        :   CGFloat  =   255.0
        var alpha       :   CGFloat  =   1.0

        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        return NSString(format: "#%02lX%02lX%02lX", lround(Double(red) * 255), lround(Double(green) * 255), lround(Double(blue) * 255)) as String
    }

    func invert() -> UIColor {
		var red         :   CGFloat  =   255.0
		var green       :   CGFloat  =   255.0
		var blue        :   CGFloat  =   255.0
		var alpha       :   CGFloat  =   1.0

		self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)

		red     =   255.0 - (red * 255.0)
		green   =   255.0 - (green * 255.0)
		blue    =   255.0 - (blue * 255.0)

		return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
	}

    func invertGrayScale() -> UIColor {

		let grayScale = self.grayScale()

		var whiteColor	:	CGFloat		=	255.0
		var alpha       :   CGFloat		=	1.0

		grayScale.getWhite(&whiteColor, alpha: &alpha)

		whiteColor	= 255.0 - (whiteColor * 255.0)
		alpha		= 255.0 - (alpha * 255.0)

		if whiteColor < 125 {
			return UIColor(white: 0.9, alpha: alpha)
		} else {
			return UIColor(white: 0.1, alpha: alpha)
		}
	}

    func grayScale() -> UIColor {
		var red         :   CGFloat  =   1.0
		var green       :   CGFloat  =   1.0
		var blue        :   CGFloat  =   1.0
		var alpha       :   CGFloat  =   1.0

		self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)

		red     =   1.0 - red
		green   =   1.0 - green
		blue    =   1.0 - blue
		alpha	=	1.0 - alpha

//		R' = G' = B' = (R+G+B) / 3
//		return UIColor(white: (red + green + blue) / 3, alpha: alpha)
//		Or
//		R' = G' = B' = 0.2126R + 0.7152G + 0.0722B
		return UIColor(white: (0.2126 * red) + (0.7152 * green) + (0.0722 * blue), alpha: alpha)
//		Or
//		R' = G' = B'  = 0.299R + 0.587G + 0.114B
//		return UIColor(white: (0.299 * red) + (0.587 * green) + (0.114 * blue), alpha: alpha)
	}

    func alphaScale(_ alpha: CGFloat) -> UIColor {
        var red         :   CGFloat  =   1.0
        var green       :   CGFloat  =   1.0
        var blue        :   CGFloat  =   1.0

        self.getRed(&red, green: &green, blue: &blue, alpha: nil)

        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }

    class var random: UIColor {

        let red = CGFloat(arc4random_uniform(256))
        let green = CGFloat(arc4random_uniform(256))
        let blue = CGFloat(arc4random_uniform(256))

        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1)
    }
}

#endif
