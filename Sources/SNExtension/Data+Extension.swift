//
//  Data+Extension.swift
//
//  Created by Saran Nonkamjan on 6/9/2561 BE.
//  Copyright © 2561 Saran Nonkamjan. All rights reserved.
//

import Foundation

public extension Data {
    var toString: String? {
        return String(data: self, encoding: .utf8)
    }

    /// แปลง Hex Data ไห้กลายเป็น Hex String
    var hexString: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
