//
//  Error+Extension.swift
//
//  Created by Saran Nonkamjan on 12/7/2562 BE.
//  Copyright © 2562 Saran Nonkamjan. All rights reserved.
//

import Foundation

public extension Error {
    var code: Int {
        return (self as NSError).code
    }
}
