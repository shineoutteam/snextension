//
//  UIFont+Extension.swift
//
//  Created by Saran Nonkamjan on 1/11/2559 BE.
//  Copyright © 2559 Saran Nonkamjan. All rights reserved.
//

import Foundation

#if canImport(UIKit)

import UIKit

public protocol FontName {
    var name: String { get }
}

public extension UIFont {

    class func name(name: FontName, size: CGFloat) -> UIFont {
        return UIFont(name: name.name, size: size) ?? UIFont.systemFont(ofSize: size)
    }

    func sizeOfString (_ string: String, constrainedToWidth width: Double) -> CGSize {
        if string == "" {
            return CGSize.zero
        }

        return (string as NSString).boundingRect(with: CGSize(width: width, height: Double.greatestFiniteMagnitude),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [.font: self],
            context: nil).size
    }

    func sizeOfString (_ string: String, constrainedToWidth width: Double, height: Double) -> CGSize {

        if string == "" {
            return CGSize.zero
        }

        return (string as NSString).boundingRect(with: CGSize(width: width, height: height),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [.font: self],
            context: nil).size
    }
}

#endif
