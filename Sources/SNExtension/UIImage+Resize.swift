//
//  UIImage+Resize.swift
//
//  Created by Saran Nonkamjan on 3/22/2560 BE.
//  Copyright © 2560 ShineOut Co.,Ltd. All rights reserved.
//

#if canImport(UIKit)

import Foundation
import UIKit

public extension UIImage {

    static let MIN_UPLOAD_IMAGE_SIZE: CGFloat = 1024
    static let IMAGE_REPRESENTATION: CGFloat = 0.5

    private func rad(_ angle: Double) -> CGFloat {
        return CGFloat(angle * Double.pi / 180.0)
    }

    private func orientationTransformedRectOfImage() -> CGAffineTransform {
        switch self.imageOrientation {
        case .left:
            return CGAffineTransform(rotationAngle: rad(90)).translatedBy(x: 0, y: -self.size.height)
        case .right:
            return CGAffineTransform(rotationAngle: rad(-90)).translatedBy(x: -self.size.width, y: 0)
        case .down:
            return CGAffineTransform(rotationAngle: rad(-180)).translatedBy(x: -self.size.width, y: -self.size.height)
        default:
            return CGAffineTransform.identity
        }
    }

    func crop(withRect cropRect:CGRect) -> UIImage {
        let rectTransform: CGAffineTransform = self.orientationTransformedRectOfImage()
        let cropRect = cropRect.applying(rectTransform)

        guard let cgImage = self.cgImage else {
            return self
        }

        guard let imageRef = cgImage.cropping(to: cropRect) else {
            return self
        }

        let newImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        return newImage
    }

    func cropImageToSqure() -> UIImage {
        let size = self.size
        var startCropPoint = CGPoint.zero
        if size.width > size.height {
            startCropPoint.x = (size.width - size.height) / 2
        } else {
            startCropPoint.y = (size.height - size.width) / 2
        }

        var pictureSize: CGFloat = 0
        if size.width > size.height {
            pictureSize = size.height
        } else {
            pictureSize = size.width
        }

        // WTF: Don't forget that the CGImageCreateWithImageInRect believes that
        // the image is 180 rotated, so x and y are inverted, same for height and width.
        let cropRect = CGRect(x: startCropPoint.x, y: startCropPoint.y, width: pictureSize, height: pictureSize)

        guard let cgImage = self.cgImage else {
            return self
        }

        guard let imageRef = cgImage.cropping(to: cropRect) else {
            return self
        }

        let newImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)

        return newImage
    }

    func resizeImage(newWidth: CGFloat) -> UIImage {

        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContextWithOptions(CGSize(width: newWidth,height: newHeight), false, UIScreen.main.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        if newImage != nil {
            return newImage!
        } else {
            return self
        }
    }

    func resizeImage(newHeight: CGFloat) -> UIImage {

        let scale = newHeight / self.size.height
        let newWidth = self.size.width * scale
        UIGraphicsBeginImageContextWithOptions(CGSize(width: newWidth,height: newHeight), false, UIScreen.main.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        if newImage != nil {
            return newImage!
        } else {
            return self
        }
    }

    func resizeImageRequire() -> UIImage {

        if self.size.height > UIImage.MIN_UPLOAD_IMAGE_SIZE && self.size.width > UIImage.MIN_UPLOAD_IMAGE_SIZE {
            let minSize = min(self.size.height, self.size.width)
            let ratio = UIImage.MIN_UPLOAD_IMAGE_SIZE / minSize

            if let oldImage: UIImage = self.copy() as? UIImage {
                return self.resizeImage(newWidth: oldImage.size.width*ratio)
            } else {
                return self;
            }
        }
        return self;
    }

    func rotateImageWithOrientation(orientation: UIImage.Orientation) -> UIImage {
        var newwOrientation = self.imageOrientation
        switch self.imageOrientation {
        case .up:
            newwOrientation = orientation
        case .upMirrored:
            switch orientation {
            case .up:
                newwOrientation = .upMirrored
            case .upMirrored:
                newwOrientation = .up
            case .down:
                newwOrientation = .downMirrored
            case .downMirrored:
                newwOrientation = .down
            case .left:
                newwOrientation = .leftMirrored
            case .leftMirrored:
                newwOrientation = .left
            case .right:
                newwOrientation = .rightMirrored
            case .rightMirrored:
                newwOrientation = .right
            @unknown default:
                assert(false, #function)
            }
        case .down:
            switch orientation {
            case .up:
                newwOrientation = .down
            case .upMirrored:
                newwOrientation = .downMirrored
            case .down:
                newwOrientation = .up
            case .downMirrored:
                newwOrientation = .upMirrored
            case .left:
                newwOrientation = .right
            case .leftMirrored:
                newwOrientation = .rightMirrored
            case .right:
                newwOrientation = .left
            case .rightMirrored:
                newwOrientation = .leftMirrored
            @unknown default:
                assert(false, #function)
            }
        case .downMirrored:
            switch orientation {
            case .up:
                newwOrientation = .downMirrored
            case .upMirrored:
                newwOrientation = .down
            case .down:
                newwOrientation = .upMirrored
            case .downMirrored:
                newwOrientation = .up
            case .left:
                newwOrientation = .rightMirrored
            case .leftMirrored:
                newwOrientation = .right
            case .right:
                newwOrientation = .leftMirrored
            case .rightMirrored:
                newwOrientation = .left
            @unknown default:
                assert(false, #function)
            }
        case .left:
            switch orientation {
            case .up:
                newwOrientation = .left
            case .upMirrored:
                newwOrientation = .leftMirrored
            case .down:
                newwOrientation = .right
            case .downMirrored:
                newwOrientation = .rightMirrored
            case .left:
                newwOrientation = .down
            case .leftMirrored:
                newwOrientation = .downMirrored
            case .right:
                newwOrientation = .up
            case .rightMirrored:
                newwOrientation = .upMirrored
            @unknown default:
                assert(false, #function)
            }
        case .leftMirrored:
            switch orientation {
            case .up:
                newwOrientation = .leftMirrored
            case .upMirrored:
                newwOrientation = .left
            case .down:
                newwOrientation = .rightMirrored
            case .downMirrored:
                newwOrientation = .right
            case .left:
                newwOrientation = .downMirrored
            case .leftMirrored:
                newwOrientation = .down
            case .right:
                newwOrientation = .upMirrored
            case .rightMirrored:
                newwOrientation = .up
            @unknown default:
                assert(false, #function)
            }
        case .right:
            switch orientation {
            case .up:
                newwOrientation = .right
            case .upMirrored:
                newwOrientation = .rightMirrored
            case .down:
                newwOrientation = .left
            case .downMirrored:
                newwOrientation = .leftMirrored
            case .left:
                newwOrientation = .up
            case .leftMirrored:
                newwOrientation = .upMirrored
            case .right:
                newwOrientation = .down
            case .rightMirrored:
                newwOrientation = .downMirrored
            @unknown default:
                assert(false, #function)
            }
        case .rightMirrored:
            switch orientation {
            case .up:
                newwOrientation = .rightMirrored
            case .upMirrored:
                newwOrientation = .right
            case .down:
                newwOrientation = .leftMirrored
            case .downMirrored:
                newwOrientation = .left
            case .left:
                newwOrientation = .upMirrored
            case .leftMirrored:
                newwOrientation = .up
            case .right:
                newwOrientation = .downMirrored
            case .rightMirrored:
                newwOrientation = .down
            @unknown default:
                assert(false, #function)
            }
        @unknown default:
            assert(false, #function)
        }

        let size = self.size

        if orientation == .up || orientation == .down || orientation == .upMirrored || orientation == .downMirrored {
            UIGraphicsBeginImageContextWithOptions(CGSize(width: size.width,height: size.height), false, UIScreen.main.scale)

            guard let cgImage = self.cgImage else {
                return self
            }

            UIImage(cgImage: cgImage, scale: 1.0, orientation: newwOrientation).draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        } else {
            UIGraphicsBeginImageContextWithOptions(CGSize(width: size.height,height: size.width), false, UIScreen.main.scale)

            guard let cgImage = self.cgImage else {
                return self
            }

            UIImage(cgImage: cgImage, scale: 1.0, orientation: newwOrientation).draw(in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        }

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        if newImage != nil {
            return newImage!
        } else {
            return self
        }
    }
}

#endif
