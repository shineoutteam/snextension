//
//  UIDevice+Extension.swift
//
//  Created by Saran Nonkamjan on 12/7/2561 BE.
//  Copyright © 2561 Saran Nonkamjan. All rights reserved.
//

#if canImport(UIKit)

import UIKit

public extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
}

#endif
