//
//  String+Extension.swift
//
//  Created by Saran Nonkamjan on 1/3/2559 BE.
//  Copyright © 2559 Saran Nonkamjan. All rights reserved.
//

import Foundation

#if canImport(UIKit)

import UIKit

public extension String {

    var CGFloatValue: CGFloat {
        return CGFloat(self.floatValue)
    }

    var floatValue: Float {
        return (self as NSString).floatValue
    }

    var intValue: Int32 {
        return (self as NSString).intValue
    }

    var integerValue: NSInteger {
        return (self as NSString).integerValue
    }

    var doubleValue: Double {
        return (self as NSString).doubleValue
    }

    var boolValue: Bool {
        return (self as NSString).boolValue
    }

    var NSStringValue: NSString {
        return self as NSString
    }

    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }

    func substring(_ from: Int) -> String {
        let index = self.index(startIndex, offsetBy: from)
        let substring = self[index...]
        return String(substring)
    }

    var length: Int {
        return self.count
    }

    var underline: NSAttributedString {
        return NSAttributedString(string: self, attributes: [.underlineStyle: 1])
    }

    func trimLeadingAndTrailingSpaces() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }

    var data: Data {
        return self.data(using: String.Encoding.utf8) ?? Data()
    }

    /// แปลง Hex String ไห้กลายเป็น Hex Data
    var hexData: Data? {
        var data = Data(capacity: self.count / 2)

        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }

        guard data.count > 0 else { return nil }

        return data
    }

    func stringByAddingPercentEncodingForRFC3986() -> String? {
        let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return self.NSStringValue.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
    }

    func containsLink() -> Bool {
        return !containedLinks().isEmpty
    }

    typealias URLWithRange = (URL: URL, range: NSRange)

    func containedLinks() -> [URLWithRange] {
        let linkDetector : NSDataDetector? = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)

        let range = NSRange(location: 0, length: self.count)
        guard let matches = linkDetector?.matches(in: self, options: [], range: range) else { return [] }
        return matches.compactMap {
            guard let url = $0.url else { return nil }
            return (url, $0.range)
        }
    }

    func isNumber() -> Bool {
        let notDigits: CharacterSet = CharacterSet.decimalDigits.inverted
        if self.NSStringValue.rangeOfCharacter(from: notDigits).location == NSNotFound {
            // newString consists only of the digits 0 through 9
            return true
        } else {
            return false
        }
    }
}

#endif
