//
//  String+HTML.swift
//
//  Created by Saran Nonkamjan on 2/17/2560 BE.
//  Copyright © 2560 ShineOut. All rights reserved.
//

#if canImport(UIKit)

import Foundation
import UIKit

public extension String {

    // MARK: - Helpers
    var html2AttributedString: NSMutableAttributedString? {
        guard data(using: .utf8) != nil else { return nil }
        do {
            return try NSMutableAttributedString(data: self.data,
                           options: [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil).trimmedLastAttributedString(set: CharacterSet.whitespacesAndNewlines)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }

    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}

public extension NSMutableAttributedString {

    func trimmedLastAttributedString(set: CharacterSet) -> NSMutableAttributedString {

        let invertedSet = set.inverted

        var range = (string as NSString).rangeOfCharacter(from: invertedSet)
        let loc = range.length > 0 ? range.location : 0

        range = (string as NSString).rangeOfCharacter(
            from: invertedSet, options: .backwards)
        let len = (range.length > 0 ? NSMaxRange(range) : string.count) - loc

        let r = self.attributedSubstring(from: NSMakeRange(loc, len))
        return NSMutableAttributedString(attributedString: r)
    }
}

#endif
