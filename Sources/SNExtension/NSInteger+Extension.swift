//
//  NSInteger+Extension.swift
//
//  Created by Saran Nonkamjan on 2/3/2559 BE.
//  Copyright © 2559 Saran Nonkamjan. All rights reserved.
//

import Foundation
import CoreGraphics

public extension NSInteger {
    func prettyString() -> String {
        let numberFormat = NumberFormatter()
        numberFormat.formatterBehavior = .behavior10_4
        numberFormat.numberStyle = .decimal

        return numberFormat.string(from: NSNumber(value: self)) ?? ""
    }

    func prettyString(maxValue: Int) -> String {
        let numberFormat = NumberFormatter()
        numberFormat.formatterBehavior = .behavior10_4
        numberFormat.numberStyle = .decimal

        if self > maxValue {
            return (numberFormat.string(from: NSNumber(value: maxValue)) ?? "").appending("+")
        } else {
            return numberFormat.string(from: NSNumber(value: self)) ?? ""
        }
    }

    func prettyString(_ currencyCode: String?) -> String {
        guard currencyCode != nil && currencyCode != "" else {
            return prettyString()
        }

        let numberFormat = NumberFormatter()
        numberFormat.formatterBehavior = .behavior10_4
        numberFormat.numberStyle = .currency
        numberFormat.currencyCode = currencyCode

        return numberFormat.string(from: NSNumber(value: self)) ?? ""
    }
}


public extension Double {
    func prettyString() -> String {
        let numberFormat = NumberFormatter()
        numberFormat.formatterBehavior = .behavior10_4
        numberFormat.numberStyle = .decimal

        return numberFormat.string(from: NSNumber(value: self)) ?? ""
    }
}

public extension Float {
    func prettyString() -> String {
        let numberFormat = NumberFormatter()
        numberFormat.formatterBehavior = .behavior10_4
        numberFormat.numberStyle = .decimal
        numberFormat.maximumFractionDigits = 2

        return numberFormat.string(from: NSNumber(value: self)) ?? ""
    }
}

public extension CGFloat {
    func prettyString() -> String {
        return Float(self).prettyString()
    }
}
